.PHONY: all sync  sync-clean clean superclean                                              \
	artifact-generatorUI artifact-generatorUI-clean artifact-generatorUI-superclean        \
    artifact-docker-build artifact-docker-publish                                          \
	GoPackages GoPackages-clean GoPackages-superclean
	
AG_BASEDIR ?= $(shell pwd)
BUILDDIR_AIKAAN ?= $(AG_BASEDIR)/build
AG_BUILDDIR = $(BUILDDIR_AIKAAN)
GOPATH ?= $(AG_BUILDDIR)/go

PKGWEBDIR=$(AG_BUILDDIR)/agweb
LABEL ?= dev
AIKAAN_DOCKER_REGISTRY ?= aikaan

export AG_BUILDDIR GOPATH

# Don't put anything on all
all: docker-build

docker-publish: docker-build
	docker push $(AIKAAN_DOCKER_REGISTRY)/mender-artifact-tool:$(LABEL)

docker-build: GoPackages generatorUI
	docker build -t $(AIKAAN_DOCKER_REGISTRY)/mender-artifact-tool:$(LABEL) .

sync::
	mkdir -p $(PKGWEBDIR)
	mkdir -p $(GOPATH)/src  $(GOPATH)/bin
	mkdir -p $(AG_BUILDDIR)/bin $(AG_BUILDDIR)/etc
	@echo "Nothing to sync for artifact-generator ..."

GoPackages: sync
	make -C GoPackages
	#cp -r ./GoPackages/$(MODULECFGDIR)/*  $(AG_BUILDDIR)/etc/

GoPackages-clean:
	make -C GoPackages clean

GoPackages-superclean:
	make -C GoPackages superclean

generatorUI: sync
	make -C artifact-generatorUI
	cp -r ./artifact-generatorUI/build/*  $(PKGWEBDIR)

generatorUI-clean:
	make -C artifact-generatorUI clean

artifact-generatorUI-superclean:
	make -C artifact-generatorUI superclean


clean: sync-clean GoPackages-clean artifact-generatorUI-clean \

superclean: sync-clean GoPackages-superclean artifact-generatorUI-superclean \
