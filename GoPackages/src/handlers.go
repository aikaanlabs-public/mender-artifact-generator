package main

import (
	"log"
	"net/http"
	"os"
	"os/exec"
)

const secret_key = "ddfsfds"

func CreateArtifact(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(32 << 20)

	//captchaResponse := r.FormValue("g-recaptcha-response")
	//data, err := json.Marshal(map[string]string{
	//	"secret":   secret_key,
	//	"response": captchaResponse,
	//	"remoteip": GetClientIP(r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR")),
	//})
	//
	//_, err = http.Post("https://www.google.com/recaptcha/api/siteverify", "application/json", bytes.NewBuffer(data))
	//if err != nil {
	//	log.Println("Captcha not validated")
	//	http.Error(w, err.Error(), http.StatusNotAcceptable)
	//	return
	//}

	ArtifactType := r.FormValue("ArtifactType")
	ArtifactName := r.FormValue("ArtifactName")
	DeviceType := r.FormValue("DeviceType")

	StoredFileDir := CreateTempDir()

	StoredArtifactPath := CheckSuffix(StoredFileDir) + ArtifactName + ".mender"

	var cmd *exec.Cmd
	//creating the Artifact by running the script
	switch ArtifactType {
	case "file":
		DestDir := r.FormValue("DestDir")

		FileName, err := ParseAndUploadFile(r, StoredFileDir, "file")
		if err != nil {
			log.Println("error in uploading file")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		StoredFilePath := CheckSuffix(StoredFileDir) + FileName

		cmd = exec.Command("/usr/local/bin/single-file-artifact-gen.sh", "-n", ArtifactName, "-t", DeviceType, "-d", DestDir, "-o", StoredArtifactPath, StoredFilePath)

	case "script":

		ShellScriptName, err := ParseAndUploadFile(r, StoredFileDir, "shellScript")
		if err != nil {
			log.Println("error in uploading shell script")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		StoredShellScriptsPath := CheckSuffix(StoredFileDir) + ShellScriptName

		cmd = exec.Command("/usr/local/bin/mender-artifact", "write", "module-image", "-T", "script", "-n", ArtifactName, "-t", DeviceType, "-o", StoredArtifactPath, "-f", StoredShellScriptsPath)

	case "Aikaan-OTA":
		DestDir := r.FormValue("DestDir")

		FileName, err := ParseAndUploadFile(r, StoredFileDir, "file")
		if err != nil {
			log.Println("error in uploading file")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		ShellScriptName, err := ParseAndUploadFile(r, StoredFileDir, "shellScript")
		if err != nil {
			log.Println("error in uploading shell script")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		StoredFilePath := CheckSuffix(StoredFileDir) + FileName
		StoredShellScriptsPath := CheckSuffix(StoredFileDir) + ShellScriptName

		cmd = exec.Command("/usr/local/bin/aikaan-ota-generator.sh", "-n", ArtifactName, "-t", DeviceType, "-d", DestDir, "-o", StoredArtifactPath, "--file-path", StoredFilePath, "--script-path", StoredShellScriptsPath)

	case "Aikaan-OTA-Reboot":
		DestDir := r.FormValue("DestDir")

		FileName, err := ParseAndUploadFile(r, StoredFileDir, "file")
		if err != nil {
			log.Println("error in uploading file")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		ShellScriptName, err := ParseAndUploadFile(r, StoredFileDir, "shellScript")
		if err != nil {
			log.Println("error in uploading shell script")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		StoredFilePath := CheckSuffix(StoredFileDir) + FileName
		StoredShellScriptsPath := CheckSuffix(StoredFileDir) + ShellScriptName

		cmd = exec.Command("/usr/local/bin/aikaan-ota-reboot-generator.sh", "-n", ArtifactName, "-t", DeviceType, "-d", DestDir, "-o", StoredArtifactPath, "--file-path", StoredFilePath, "--script-path", StoredShellScriptsPath)

	case "Apk":
		AppType := r.FormValue("AppType")

		FileName, err := ParseAndUploadFile(r, StoredFileDir, "file")
		if err != nil {
			log.Println("error in uploading file")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		StoredFilePath := CheckSuffix(StoredFileDir) + FileName

		cmd = exec.Command("/usr/local/bin/aikaan-apk-install-upgrade-artifact-generator.sh", "-n", ArtifactName, "-t", DeviceType, "-o", StoredArtifactPath, "-f", StoredFilePath, "-at", AppType)

	case "debian":
		FileName, err := ParseAndUploadFile(r, StoredFileDir, "file")
		if err != nil {
			log.Println("error in uploading file")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		StoredFilePath := CheckSuffix(StoredFileDir) + FileName

		cmd = exec.Command("/usr/local/bin/mender-artifact", "write", "module-image", "--type", "deb", "-n", ArtifactName, "-t", DeviceType, "-o", StoredArtifactPath, "--file", StoredFilePath)

	case "root-fs":
		FileName, err := ParseAndUploadFile(r, StoredFileDir, "file")
		if err != nil {
			log.Println("error in uploading file")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		StoredFilePath := CheckSuffix(StoredFileDir) + FileName

		cmd = exec.Command("/usr/local/bin/mender-artifact", "write", "rootfs-image", "-n", ArtifactName, "-t", DeviceType, "-o", StoredArtifactPath, "--file", StoredFilePath)
	}

	out, err := cmd.Output()
	if err != nil {
		println("Error in output")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	print(string(out))

	//downloading the artifact and parsing the artifact data to client
	DownloadArtifact(w, StoredArtifactPath, ArtifactName+".mender")

	//removing temp directory
	defer os.RemoveAll(StoredFileDir)
	return
}
