package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	uuid "github.com/google/uuid"
)

const DEFAULT_TMP_DIR = "/tmp/"

const DEFAULT_FILE_SIZE_LIMIT_BYTES = 100 * 1024 * 1024 // 100 mb

func FileSizeUnderLimit(fileSize int64) bool {
	if fileSize > DEFAULT_FILE_SIZE_LIMIT_BYTES {
		return false
	} else {
		return true
	}
}
func CheckSuffix(path string) string {
	if !strings.HasSuffix(path, "/") {
		return path + "/"
	}
	return path
}

func GenerateUUID() string {
	s := uuid.New().String()
	_, err := uuid.Parse(s)
	if err != nil {
		fmt.Printf("Something went wrong: %s", err)
		return ""
	}
	return s
}

func CreateTempDir() string {

	uuid := GenerateUUID()

	tmp := os.Getenv("TMP")
	if len(tmp) == 0 {
		tmp = DEFAULT_TMP_DIR
	}
	//create a temp directory with name as uuid in the temp directory
	StoredFileDir, err := ioutil.TempDir(tmp, uuid)
	if err != nil {
		log.Fatal(err)
	}

	return StoredFileDir
}

func GetClientIP(remoteAddr string, xForwarded string) string {
	var ip_addr string

	if len(remoteAddr) == 0 {
		ip_addr = xForwarded
	} else {
		ip_addr = remoteAddr
	}
	return ip_addr
}

func ParseAndUploadFile(r *http.Request, StoredFileDir string, fileType string) (string, error) {

	file, fileHandler, err := r.FormFile(fileType)
	if err != nil {
		log.Println("error in formFile")
		return "", err
	}
	defer file.Close()

	fileSize := fileHandler.Size
	if !FileSizeUnderLimit(fileSize) {
		log.Println("File size bigger than allowed")
		return "", err
	}

	FileName := fileHandler.Filename

	//creating the file
	f, err := os.OpenFile(CheckSuffix(StoredFileDir)+FileName, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Println(err)
		return "", err
	}

	defer f.Close()

	//copying the data to the file
	io.Copy(f, file)

	return FileName, nil

}

func DownloadArtifact(w http.ResponseWriter, StoredArtifactPath string, Filename string) {

	//opening the stored file
	Openfile, err := os.Open(StoredArtifactPath)
	if err != nil {
		http.Error(w, "File not found.", 404)
		log.Fatal("file not found")
	}

	defer Openfile.Close()

	//reading the file headers
	FileHeader := make([]byte, 512)
	Openfile.Read(FileHeader)
	FileContentType := http.DetectContentType(FileHeader)
	FileStat, _ := Openfile.Stat()                     //Get info from file
	FileSize := strconv.FormatInt(FileStat.Size(), 10) //Get file size as a string

	//Send the headers
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Disposition", "attachment; filename="+Filename)
	w.Header().Set("Content-Type", FileContentType)
	w.Header().Set("Content-Length", FileSize)

	//Send the file
	//We read 512 bytes from the file already so we reset the offset back to 0
	Openfile.Seek(0, 0)

	//copying the data to the file
	io.Copy(w, Openfile)
}
