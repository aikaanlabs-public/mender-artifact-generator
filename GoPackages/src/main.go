package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {

	router := mux.NewRouter()

	router.HandleFunc("/api/create", CreateArtifact).Methods("POST")

	httpConnString := ":8080"
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization", "Access-Control-Allow-Origin"})
	originsOk := handlers.AllowedOrigins([]string{os.Getenv("CORS_ALLOWED_ORIGIN")})
	methodsOk := handlers.AllowedMethods([]string{"DELETE", "GET", "HEAD", "OPTIONS", "POST", "PUT"})
	credsOK := handlers.AllowCredentials()

	server := &http.Server{
		Addr:    httpConnString,
		Handler: handlers.CORS(headersOk, originsOk, methodsOk, credsOK)(router),
	}
	log.Printf("Server listening on: %s\n", httpConnString)

	//serving the static files
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("/var/www/static"))))

	router.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "/var/www/index.html")
	})

	server.ListenAndServe()

}
