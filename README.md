**Mender Artifact Tool**

This Repository contains the source code for Mender Artifact Tool hosted at artifact.aikaan.io


**Prerequisites**

- golang
- react-js
- docker
- docker-compose


**How to make changes**

Clone the repository
For FrontEnd changes, make changes in the artifact-generatorUI directory and for Backend, make changes in the GoPackages directory.


**How to build and run**

Build docker: `make docker-build`
Run the tool: `docker-compose up -d`

Note : Please check the **port** number in **docker-compose.yml** to know which port the UI is being rendered at


