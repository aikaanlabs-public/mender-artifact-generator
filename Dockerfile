FROM alpine:3.11

COPY build/bin/ag /usr/bin/

COPY build/agweb/ /var/www/

COPY Scripts/ /usr/local/bin/

ENTRYPOINT [ "/usr/bin/ag" ]