#!/bin/sh

set -e

# TODO:
# This is a temporary script. Remove this and add a flag to aikaan-ota-generator to decide whether to generate an artifact with reboot or not

show_help() {
  cat << EOF

Simple tool to generate Mender Artifact suitable for aikaan-ota-reboot Update Module

Usage: $0 [options] file [-- [options-for-mender-artifact] ]

    Options: [ -n|artifact-name -t|--device-type -d|--dest-dir -o|--output_path -h|--help ]

        --artifact-name     - Artifact name
        --device-type       - Target device type identification (can be given more than once)
        --dest-dir          - Target destination directory where to deploy the update
        --output-path       - Path to output file. Default: file-install-artifact.mender
        --file-path         - Path to single file
        --script-path       - Path to script file
        --help              - Show help and exit
        file                - Single file to bundle in the update
        script_file         - Script file to bundle in the update

Anything after a '--' gets passed directly to the mender-artifact tool.

Example: ./aikaan-ota-reboot-generator -n artifact_name -t armv7l -d /tmp -o test_file.mender --file-path ./single_file.txt --script-path ./exec_file.sh

EOF
}

show_help_and_exit_error() {
  show_help
  exit 1
}

check_dependency() {
  which "$1" > /dev/null || ( echo "$1 not found in PATH" && exit 1 )
}

check_dependency mender-artifact

device_types=""
artifact_name=""
dest_dir=""
output_path="aikaan-ota-reboot-artifact.mender"
dest_dir_file="dest_dir"
filename_file="filename"
script_filename_file="scriptfilename"
permissions_file="permissions"
file=""
script_file=""
passthrough=0
passthrough_args=""

while [ -n "$1" ]; do
  if test $passthrough -eq 1
  then
    passthrough_args="$passthrough_args $1"
    shift
    continue
  fi
  case "$1" in
    --device-type | -t)
      if [ -z "$2" ]; then
        show_help_and_exit_error
      fi
      device_types="$device_types $1 $2"
      shift 2
      ;;
    --artifact-name | -n)
      if [ -z "$2" ]; then
        show_help_and_exit_error
      fi
      artifact_name=$2
      shift 2
      ;;
    --dest-dir | -d)
      if [ -z "$2" ]; then
        show_help_and_exit_error
      fi
      dest_dir=$2
      shift 2
      ;;
    --output-path | -o)
      if [ -z "$2" ]; then
        show_help_and_exit_error
      fi
      output_path=$2
      shift 2
      ;;
    --file-path | -f)
      if [ -z "$2" ]; then
        show_help_and_exit_error
      fi
      file=$2
      shift 2
      ;;
    --script-path | -s)
      if [ -z "$2" ]; then
        show_help_and_exit_error
      fi
      script_file=$2
      shift 2
      ;;
    -h | --help)
      show_help
      exit 0
      ;;
    --)
      passthrough=1
      shift
      ;;
    -*)
      echo "Error: unsupported option $1"
      show_help_and_exit_error
      ;;
    *)
      echo "Error: unsupported option $1"
      show_help_and_exit_error
      ;;
  esac
done

if [ -z "${artifact_name}" ]; then
  echo "Artifact name not specified. Aborting."
  show_help_and_exit_error
fi

if [ -z "${device_types}" ]; then
  echo "Device type not specified. Aborting."
  show_help_and_exit_error
fi

if [ -z "${dest_dir}" ]; then
  echo "Destination dir not specified. Aborting."
  show_help_and_exit_error
fi

if [ -z "${file}" ]; then
  echo "File not specified. Aborting."
  show_help_and_exit_error
fi
if [ -z "${script_file}" ]; then
  echo "Script File not specified. Aborting."
  show_help_and_exit_error
fi

# Check dest-dir is an absolute path
case $dest_dir in
  /*)
    ;;
  *)
    echo "Destination dir must be an absolute path. Aborting"
    exit 1
  ;;
esac

# Create tarball, accepts single file or directory.
filename=""
if [ -e "${file}" ]; then
  if [ -f "${file}" ]; then
    filename=$(basename $file)
  else
    echo "Error: \"${file}\" is not a regular file. Aborting."
    exit 1
  fi
else
  echo "Error: File \"${file}\" does not exist. Aborting."
  exit 1
fi

# Create tarball, accepts single file or directory.
script_filename=""
if [ -e "${script_file}" ]; then
  if [ -f "${script_file}" ]; then
    script_filename=$(basename $script_file)
  else
    echo "Error: \"${script_file}\" is not a regular file. Aborting."
    exit 1
  fi
else
  echo "Error: File \"${script_file}\" does not exist. Aborting."
  exit 1
fi


# Create dest_dir file in plain text
echo "$dest_dir" > $dest_dir_file

# Create single_file file in plain text
echo "$filename" > $filename_file

# Create single_file file in plain text
echo "$script_filename" > $script_filename_file

STAT_HAS_f=1;
stat -f %A "${file}" >/dev/null 2>&1 || STAT_HAS_f=0;

# Create permissions file in plain text
if [ $STAT_HAS_f -eq 1 ]; then
  stat -f %A "${file}" > $permissions_file
else
  stat -c %a "${file}" > $permissions_file
fi

mender-artifact write module-image \
  -T aikaan-ota-reboot \
  $device_types \
  -o $output_path \
  -n $artifact_name \
  -f $dest_dir_file \
  -f $filename_file \
  -f $script_filename_file \
  -f $permissions_file \
  -f $file \
  -f $script_file \
  $passthrough_args

rm $dest_dir_file
rm $filename_file
rm $script_filename_file
rm $permissions_file

echo "Artifact $output_path generated successfully:"
mender-artifact read $output_path

exit 0

