#!/bin/sh

set -e

show_help() {
  cat << EOF

Simple tool to generate Mender Artifact suitable for aikaan-ota-reboot Update Module

Usage: $0 [options] file [-- [options-for-mender-artifact] ]

    Options: [ -n|artifact-name -t|--device-type -o|--output_path -f|--apk_file-path -at|app_type -h|--help ]

        --artifact-name     - Artifact name
        --device-type       - Target device type identification (can be given more than once)
        --output-path       - Path to output file. Default: file-install-artifact.mender
        --apk_file-path     - Path to apk file
        --app_type          - Application type (0 for user apps and 1 for system apps)
        --help              - Show help and exit
        apk_file            - Single apk file to bundle in the update

Anything after a '--' gets passed directly to the mender-artifact tool.

Example: ./aikaan-apk-install-upgrade-artifact-generator -n artifact_name -t armv7l -o aikaan-apk-artifact.mender -f ./app.apk -at 0

EOF
}

show_help_and_exit_error() {
  show_help
  exit 1
}

check_dependency() {
  which "$1" > /dev/null || ( echo "$1 not found in PATH" && exit 1 )
}

check_dependency mender-artifact

device_types=""
artifact_name=""
output_path="aikaan-apk-artifact.mender"
app_name_file="app_name"
app_type_file="app_type"
apk_file=""
passthrough=0
passthrough_args=""

while [ -n "$1" ]; do
  if test $passthrough -eq 1
  then
    passthrough_args="$passthrough_args $1"
    shift
    continue
  fi
  case "$1" in
    --device-type | -t)
      if [ -z "$2" ]; then
        show_help_and_exit_error
      fi
      device_types="$device_types $1 $2"
      shift 2
      ;;
    --artifact-name | -n)
      if [ -z "$2" ]; then
        show_help_and_exit_error
      fi
      artifact_name=$2
      shift 2
      ;;
    --output-path | -o)
      if [ -z "$2" ]; then
        show_help_and_exit_error
      fi
      output_path=$2
      shift 2
      ;;
    --apk_file-path | -f)
      if [ -z "$2" ]; then
        show_help_and_exit_error
      fi
      apk_file=$2
      shift 2
      ;;
    --app_type | -at)
      if [ -z "$2" ]; then
        show_help_and_exit_error
      fi
      app_type=$2
      shift 2
      ;;
    -h | --help)
      show_help
      exit 0
      ;;
    --)
      passthrough=1
      shift
      ;;
    -*)
      echo "Error: unsupported option $1"
      show_help_and_exit_error
      ;;
    *)
      echo "Error: unsupported option $1"
      show_help_and_exit_error
      ;;
  esac
done

if [ -z "${artifact_name}" ]; then
  echo "Artifact name not specified. Aborting."
  show_help_and_exit_error
fi

if [ -z "${device_types}" ]; then
  echo "Device type not specified. Aborting."
  show_help_and_exit_error
fi

if [ -z "${apk_file}" ]; then
  echo "apk_file not specified. Aborting."
  show_help_and_exit_error
fi

if [ -z "${app_type}" ]; then
  echo "app_type not specified. Aborting."
  show_help_and_exit_error
fi

if [ ${app_type} -gt 1 -o ${app_type} -lt 0  ]; then
  echo "app_type not supported... supported app_types are 0 and 1"
  show_help_and_exit_error
fi

# Create tarball, accepts single file or directory.
app_name=""
if [ -e "${apk_file}" ]; then
  if [ -f "${apk_file}" ]; then
    app_name=$(basename $apk_file)
  else
    echo "Error: \"${apk_file}\" is not a regular apk_file. Aborting."
    exit 1
  fi
else
  echo "Error: apk_file \"${apk_file}\" does not exist. Aborting."
  exit 1
fi

# Create single_file file in plain text for app_name
echo "$app_name" > $app_name_file

# Create single_file file in plain text for app_type (0 for user apps, 1 for system apps)
echo "$app_type" > $app_type_file

STAT_HAS_f=1;
stat -f %A "${apk_file}" >/dev/null 2>&1 || STAT_HAS_f=0;

mender-artifact write module-image \
  -T aikaan-apk \
  $device_types \
  -o $output_path \
  -n $artifact_name \
  -f $app_name_file \
  -f $app_type_file \
  -f $apk_file \
  $passthrough_args

rm $app_name_file
rm $app_type_file

echo "Artifact $output_path generated successfully:"
mender-artifact read $output_path

exit 0

